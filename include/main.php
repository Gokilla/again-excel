<?
 use PhpOffice\PhpSpreadsheet\IOFactory;
 include __DIR__ . '/config.php';
 require $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';
    class main{
        public static $DBH;

        public static function setup(){
            include 'config.php';
            self::$DBH = new PDO($dsn, $user, $pass, $opt);
            return self::$DBH;
        }

        public static function GetOrg(){
            $sl_org = self::$DBH->query("SELECT * FROM `insult_organization`");
            while ($row_org = $sl_org->fetch()) {
                $html .= '
                    <option value="'.$row_org['oid'].'">'. $row_org['oname'] .'</option>
                ';
            }
            return $html;
        }


        public static function Export(){
            //Загрузка файлов
            $target_dir = 'upload/';
            $target_dir = $target_dir . basename($_FILES["table"]["name"]);
            move_uploaded_file($_FILES["table"]["tmp_name"], $target_dir);
            $type = substr(strrchr($target_dir, '.'), 1);
            $type = mb_convert_case($type, MB_CASE_TITLE, "UTF-8");
            // айди организации $_POST['org']
            
            //Параметры 
            $inputFileType = $type;
            $inputFileName = $_SERVER['DOCUMENT_ROOT'].'/'.$target_dir;
            //Чтение
            $reader = IOFactory::createReader($inputFileType);
            $reader->setReadDataOnly(true);
            $spreadsheet = $reader->load($inputFileName);
            $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
            $count = count($sheetData);
            $count_patient = '';
            $count_diag = '';
            if($inputFileName == 'D:/server/OSPanel/domains/sheest/upload/Densaulyk.xlsx'){
                for($row = 2; $row <= count($sheetData); $row++){
                    # A = ИИН, B = ФИО, С = НОМЕР УЧАСТКА, D = ДИАГНОЗ
                    $iin = explode(",", $sheetData[$row]['A']); // ИИН
                    $fio = explode(" ", $iin['1']); // массив фио
                    $number = explode(" ", $sheetData[$row]['B']);  //Номер участка
                    $digKey = $sheetData[$row]['C'];; //Ключь диагноза
                    //Получение дня рождения по ИИН //TODO 2000 ГОД
                    $date = "19".substr($iin['0'], 0, 2)."-".substr($iin['0'], 2, 2)."-".substr($iin['0'], 4, 2);
                        $digKey =  str_replace(' ','',$digKey); 
                    //Пустой участок
                    if(empty($sheetData[$row]['C'])){
                        $number = 'Не закреплен за участком';
                        if (!empty($iin['0']) && !empty($digKey)){

                            $sl_pat = self::$DBH->prepare("SELECT * FROM `insult_patient` WHERE `piin` = ?");
                            $sl_pat->execute([$iin['0']]);
                            $row_pat = $sl_pat->fetch();
                            if($row_pat['piin'] != $iin['0']){
                                $in_patient=self::$DBH->prepare("INSERT  INTO   `insult_patient`(`piin`, `pfname`, `plname`, `pmname`, `pdate`,  `pctime`)  VALUES (?, ?, ?, ?, ?, ?)");
                                $in_patient->execute([$iin['0'], $fio[0], $fio[1], $fio[2], $date, date("Y-m-d H:i:s")]);
                                $cp++;
                                $pid =self::$DBH->lastInsertId();
                                $pos = strrpos($digKey, ")");
                                if($pos > -1){
                                    $digKey = trim(substr($digKey, $pos + 1));
                                }
                                if(strlen($digKey)>=9){
                                    $digKey = explode(",", $digKey);
                                    foreach ($digKey as $key) {
                                        # code...
                                        $in_pDiag = self::$DBH->prepare("INSERT IGNORE INTO  `insult_patient_diagnosis` (`pid`, `mkbcode`, `mkbdate`) VALUES(?, ?, ?)");
                                        $in_pDiag->execute([$pid,$key, date('Y-m-d')]);
                                        $cd++;
                                    }
                                }else{
                                    $in_pDiag = self::$DBH->prepare("INSERT IGNORE INTO  `insult_patient_diagnosis` (`pid`, `mkbcode`, `mkbdate`) VALUES(?, ?, ?)");
                                    $in_pDiag->execute([$pid,$digKey, date('Y-m-d')]);
                                    $cd++;
                                }
                            }else{
                                $sl_PacientID = self::$DBH->prepare("SELECT `pid` FROM `insult_patient` WHERE `piin`= ?");
                                $sl_PacientID->execute([$iin['0']]);
                                $row_PatiendID = $sl_PacientID->fetch();
                                // $sl_unicDig = self::$DBH->prepare("SELECT * FROM `insult_patient_diagnosis` WHERE `mkbcode` = ? ")   
                                $pos = strrpos($digKey, ")");
                                if($pos > -1){
                                    $digKey = trim(substr($digKey, $pos + 1));
                                }
                                if(strlen($digKey)>=9){
                                    $digKey = explode(",", $digKey);
                                    foreach ($digKey as $key) {
                                        # code...
                                        $in_pDiag = self::$DBH->prepare("INSERT IGNORE INTO  `insult_patient_diagnosis` (`pid`, `mkbcode`, `mkbdate`) VALUES(?, ?, ?)");
                                        $in_pDiag->execute([$row_PatiendID['pid'],$key, date('Y-m-d')]);
                                        $cd++;
                                    }
                                }else{
                                    $in_pDiag = self::$DBH->prepare("INSERT IGNORE INTO  `insult_patient_diagnosis` (`pid`, `mkbcode`, `mkbdate`) VALUES(?, ?, ?)");
                                    $in_pDiag->execute([$row_PatiendID['pid'],$digKey, date('Y-m-d')]);
                                    $cd++;
                                }
                            }
                            //Insert patient
        #$sl_dig = self::$DBH->prepare("SELECT * FROM `insult_patient_diagnosis`");
                            $count_patient++;
                            //Получение последнего id
                            
                            //Запись Диагнозов 
                            
                                $in_territory = self::$DBH->prepare("INSERT IGNORE INTO `insult_territory` (`oid`, `tname`) VALUES(?, ?) ");
                                $in_territory->execute([$_POST['org'], $number['0']]);
            
                            
                            $sl_tid = self::$DBH->prepare("SELECT `tid` FROM `insult_territory` WHERE `tname` = ? LIMIT 1 ");
                            $sl_tid->execute([$number['0']]);
                            $row_tid = $sl_tid->fetch();
                            
                            $in_pt = self::$DBH->prepare("INSERT IGNORE INTO `insult_patient_territory` (`pid`, `tid`) VALUES(?, ?)");
                            $in_pt->execute([$pid, $row_tid['tid']]);
        
                            
                            
                        }
                            //Если несколько диагнозов запись в нужное место...
                        if(empty($iin['0']) && !empty($digKey)){
                          $pos = strrpos($digKey, ")");
                            if($pos > -1){
                                $digKey = trim(substr($digKey, $pos + 1));
                            }
                            $sl_lastGood = self::$DBH->query("SELECT `pid` FROM `insult_patient_diagnosis` ORDER BY `PID` DESC LIMIT 1 ")->fetch();
                            $in_pDiag = self::$DBH->prepare("INSERT IGNORE INTO `insult_patient_diagnosis` (`pid`, `mkbcode`, `mkbdate`) VALUES(?, ?, ?)");
                            $in_pDiag->execute([$sl_lastGood['pid'],$digKey, date('Y-m-d')]);
                            $cd++;
                        }
        
                    
                    }
        
                    $sl_alldig = self::$DBH->query("SELECT * FROM `insult_patient_diagnosis`");
                    while ($row_alldig = $sl_alldig->fetch()) {
                        #todo 
                        
                        preg_match("/([a-zA-ZА-Яа-я]+)([0-9]+)/", $row_alldig['mkbcode'], $code);
                        
                        
                        switch ($code['1']) {
                            case 'E':
                                $dgroup = '2';
                                break;    
                            case 'Е':
                                $dgroup = '2';
                                break;   
                            case 'I':
                                if($code['2'] >= '11' && $code['2']<= '11.9'){
                                    $number_i = '3';
                                }
                                if($code['2'] >= '20' && $code['2']<= '25.1'){
                                    $number_i = '4';
                                }
                                if($code['2'] >= '48' && $code['2']<= '49.1'){
                                    $number_i = '5';
                                }
                                if($code['2'] >= '69' && $code['2']<= '69.9'){
                                    $number_i = '6';
                                } 
                                $dgroup = $number_i;
                                break;    
                            
                        }

                        # code...
                        $in_pdGroup = self::$DBH->prepare("INSERT IGNORE INTO `insult_patient_dgroup` (`pid`, `did`) VALUES(?, ?)");
                        $in_pdGroup->execute([$row_alldig['pid'], $dgroup]);
                    }
                   
                    self::$DBH->query("DELETE FROM `insult_patient_diagnosis` WHERE `pid` = '0'");
                    self::$DBH->query("DELETE FROM `insult_patient_dgroup` WHERE `pid` = '0'");
                   // self::$DBH->query("DELETE FROM `insult_patient_dgroup` WHERE `did` = '0'");
                    self::$DBH->query("DELETE FROM `insult_patient_territory` WHERE `pid` = '0'");
                    echo "<script>alert('Успешно пройдено строк: $count Пациентов: $cp Диагнозов: $cd  ');location.href='http://sheest/';</script>";
                }
            }else{

            for($row = 2; $row <= count($sheetData); $row++){

               
                # A = ИИН, B = ФИО, С = НОМЕР УЧАСТКА, D = ДИАГНОЗ
                $iin = $sheetData[$row]['A']; // ИИН
                $fio = explode(" ", $sheetData[$row]['B']); // массив фио
                $number = $sheetData[$row]['C']; //Номер участка
                $digKey = $sheetData[$row]['D']; //Ключь диагноза
                //Получение дня рождения по ИИН //TODO 2000 ГОД
                $date = "19".substr($iin, 0, 2)."-".substr($iin, 2, 2)."-".substr($iin, 4, 2);
                    $digKey =  str_replace(' ','',$digKey); 
                //Пустой участок
                if(empty($sheetData[$row]['C'])){
                    $number = 'Не закреплен за участком';
                }
                // if ($inputFileName = 'Densaulyk.xlsx'){
                //     $digKey = strtok($digKey, ' ');
                //     $number = strtok($number, ' ');
                // }
                //Если не пустой иин производим запись 
                if (!empty($iin) && !empty($digKey)){

                    $sl_pat = self::$DBH->prepare("SELECT * FROM `insult_patient` WHERE `piin` = ?");
                    $sl_pat->execute([$iin]);
                    $row_pat = $sl_pat->fetch();
                    if($row_pat['piin'] != $iin){
                        $in_patient=self::$DBH->prepare("INSERT  INTO   `insult_patient`(`piin`, `pfname`, `plname`, `pmname`, `pdate`,  `pctime`)  VALUES (?, ?, ?, ?, ?, ?)");
                        $in_patient->execute([$iin, $fio[0], $fio[1], $fio[2], $date, date("Y-m-d H:i:s")]);
                        $cp++;
                        $pid =self::$DBH->lastInsertId();
                        $pos = strrpos($digKey, ")");
                        if($pos > -1){
                            $digKey = trim(substr($digKey, $pos + 1));
                        }
                        if(strlen($digKey)>=9){
                            $digKey = explode(",", $digKey);
                            foreach ($digKey as $key) {
                                # code...
                                $in_pDiag = self::$DBH->prepare("INSERT IGNORE INTO  `insult_patient_diagnosis` (`pid`, `mkbcode`, `mkbdate`) VALUES(?, ?, ?)");
                                $in_pDiag->execute([$pid,$key, date('Y-m-d')]);
                                $cd++;
                            }
                        }else{
                            $in_pDiag = self::$DBH->prepare("INSERT IGNORE INTO  `insult_patient_diagnosis` (`pid`, `mkbcode`, `mkbdate`) VALUES(?, ?, ?)");
                            $in_pDiag->execute([$pid,$digKey, date('Y-m-d')]);
                            $cd++;
                        }
                    }else{
                        $sl_PacientID = self::$DBH->prepare("SELECT `pid` FROM `insult_patient` WHERE `piin`= ?");
                        $sl_PacientID->execute([$iin]);
                        $row_PatiendID = $sl_PacientID->fetch();
                        // $sl_unicDig = self::$DBH->prepare("SELECT * FROM `insult_patient_diagnosis` WHERE `mkbcode` = ? ")   
                        $pos = strrpos($digKey, ")");
                        if($pos > -1){
                            $digKey = trim(substr($digKey, $pos + 1));
                        }
                        if(strlen($digKey)>=9){
                            $digKey = explode(",", $digKey);
                            foreach ($digKey as $key) {
                                # code...
                                $in_pDiag = self::$DBH->prepare("INSERT IGNORE INTO  `insult_patient_diagnosis` (`pid`, `mkbcode`, `mkbdate`) VALUES(?, ?, ?)");
                                $in_pDiag->execute([$row_PatiendID['pid'],$key, date('Y-m-d')]);
                                $cd++;
                            }
                        }else{
                            $in_pDiag = self::$DBH->prepare("INSERT IGNORE INTO  `insult_patient_diagnosis` (`pid`, `mkbcode`, `mkbdate`) VALUES(?, ?, ?)");
                            $in_pDiag->execute([$row_PatiendID['pid'],$digKey, date('Y-m-d')]);
                            $cd++;
                        }
                    }
                    //Insert patient
#$sl_dig = self::$DBH->prepare("SELECT * FROM `insult_patient_diagnosis`");
                    $count_patient++;

                    //Запись Диагнозов 
                    
                        $in_territory = self::$DBH->prepare("INSERT IGNORE INTO `insult_territory` (`oid`, `tname`) VALUES(?, ?) ");
                        $in_territory->execute([$_POST['org'], $number]);
    
                    
                    $sl_tid = self::$DBH->prepare("SELECT `tid` FROM `insult_territory` WHERE `tname` = ? LIMIT 1 ");
                    $sl_tid->execute([$number]);
                    $row_tid = $sl_tid->fetch();
                    
                    $in_pt = self::$DBH->prepare("INSERT IGNORE INTO `insult_patient_territory` (`pid`, `tid`) VALUES(?, ?)");
                    $in_pt->execute([$pid, $row_tid['tid']]);

                    
                    
                }
                    //Если несколько диагнозов запись в нужное место...
                if(empty($iin) && !empty($digKey)){
                  $pos = strrpos($digKey, ")");
                    if($pos > -1){
                        $digKey = trim(substr($digKey, $pos + 1));
                    }
                    $sl_lastGood = self::$DBH->query("SELECT `pid` FROM `insult_patient_diagnosis` ORDER BY `PID` DESC LIMIT 1 ")->fetch();
                    $in_pDiag = self::$DBH->prepare("INSERT IGNORE INTO `insult_patient_diagnosis` (`pid`, `mkbcode`, `mkbdate`) VALUES(?, ?, ?)");
                    $in_pDiag->execute([$sl_lastGood['pid'],$digKey, date('Y-m-d')]);
                    $cd++;
                }

            
            }

            $sl_alldig = self::$DBH->query("SELECT * FROM `insult_patient_diagnosis`");
            while ($row_alldig = $sl_alldig->fetch()) {
                #todo 
                
                preg_match("/([a-zA-ZА-Яа-я]+)([0-9]+)/", $row_alldig['mkbcode'], $code);
                
                
                switch ($code['1']) {
                    case 'E':
                        $dgroup = '2';
                        break;    
                    case 'Е':
                        $dgroup = '2';
                        break;   
                    case 'I':
                        if($code['2'] >= '11' && $code['2']<= '11.9'){
                            $number_i = '3';
                        }
                        if($code['2'] >= '20' && $code['2']<= '25.1'){
                            $number_i = '4';
                        }
                        if($code['2'] >= '48' && $code['2']<= '49.1'){
                            $number_i = '5';
                        }
                        if($code['2'] >= '69' && $code['2']<= '69.9'){
                            $number_i = '6';
                        } 
                        $dgroup = $number_i;
                        break;    
                    
                }

                $in_pdGroup = self::$DBH->prepare("INSERT IGNORE INTO `insult_patient_dgroup` (`pid`, `did`) VALUES(?, ?)");
                $in_pdGroup->execute([$row_alldig['pid'], $dgroup]);
            }
           
            self::$DBH->query("DELETE FROM `insult_patient_diagnosis` WHERE `pid` = '0'");
            self::$DBH->query("DELETE FROM `insult_patient_dgroup` WHERE `pid` = '0'");
           // self::$DBH->query("DELETE FROM `insult_patient_dgroup` WHERE `did` = '0'");
            self::$DBH->query("DELETE FROM `insult_patient_territory` WHERE `pid` = '0'");
            echo "<script>alert('Успешно пройдено строк: $count Пациентов: $cp Диагнозов: $cd  ');location.href='http://sheest/';</script>";
            }
        }
}