<?
  include_once('include/main.php');
  main::setup();
  $Org = main::GetOrg();
  if(isset($_POST['upload'])){ echo main::Export();}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>upload</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <link rel="apple-touch-icon" sizes="76x76" href="/img/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon-16x16.png">
    <link rel="manifest" href="/img/site.webmanifest">
    <link rel="mask-icon" href="/img/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
</head>
<body>
<div class="row">
  <div class="col-sm-3"></div>
  <div class="col-sm-6">
    <div class="form-group" align="center">
        <form action="" method="post" enctype="multipart/form-data">
            <label for="img">Укажите Файл:</label>
            <input type="file" name="table" class="form-control" id="img">
            
            <br>
            <div class="form-group">
              <label for="sel1">Выбор организации:</label>
              <select class="form-control" name="org" >
                <?=$Org?>  
              </select>
          </div>
          <button type="submit" style="margin-top:1%" name="upload" class="btn btn-success">Загрузить</button>
        </form>
    </div>
  </div>
  <div class="col-sm-3"></div>
</div>
</body>
</html>