<?php

use PhpOffice\PhpSpreadsheet\IOFactory;

require __DIR__ . '/../Header.php';

$inputFileType = 'Xls';
$inputFileName = __DIR__ . '/sampleData/test.xls';

$helper->log('Loading file ' . pathinfo($inputFileName, PATHINFO_BASENAME) . ' using IOFactory with a defined reader type of ' . $inputFileType);
$reader = IOFactory::createReader($inputFileType);
$helper->log('Turning Formatting off for Load');
$reader->setReadDataOnly(true);
$spreadsheet = $reader->load($inputFileName);

$sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
$bad = 0;
$global = array();
for($row = 1; $row <= count($sheetData); $row++){
    # A = ИИН, B = ФИО, С = НОМЕР УЧАСТКА, D = ДИАГНОЗ
    
    
    $output['iin'] = $sheetData[$row]['A'];
    $output['fio'] = $sheetData[$row]['B']; // массив фио
    $output['place'] = $sheetData[$row]['C'];
    $output['diagnoz'] = $sheetData[$row]['D'];
    
    array_push($global, $output['iin'],$output['fio'],$output['place'], $output['diagnoz'] );   
    $date = substr($iin, 0, 6);
    $newDate = date("m-d-Y", strtotime($date));
    
    if(empty($sheetData[$row]['C'])){
        $place = 'Не закреплен за участком';
    }



    if(empty($iin) && !empty($diagnoz)){
        ++$bad;
        $rows = $row - $bad;
        $iin = $sheetData[$rows]['A'];
        $fio = explode(" ", $sheetData[$rows]['B']); // массив фио
        $place = $sheetData[$rows]['C'];
        $diagnoz = $sheetData[$rows]['D'] .= $diagnoz;    
    }

 
}
//var_dump($output);
var_dump($global);
